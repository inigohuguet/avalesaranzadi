package info.aranzadi2015.avalesaranzadi;

import info.aranzadi2015.avalesaranzadi.PrintFileHandler.PrintFileHandlerException;

import java.util.ArrayList;


import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.content.Intent;

public class DocActivity extends AvalesAranzadiBaseActivity {

	ArrayList<String> adapterArray;
	ArrayAdapter<String> adapter;
	String docName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_doc);
		// Show the Up button in the action bar.
		//setupActionBar();
		try {
			docName = getIntent().getExtras().getString("docName");
			setTitle("Doc: " + docName);
			PrintFileHandler.openDoc(docName);
			adapterArray = new ArrayList<String>();
			adapterArray.add("--- A�adir nueva p�gina");
			adapterArray.addAll(PrintFileHandler.getPages());
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, adapterArray);
			setListAdapter(adapter);
		} catch (PrintFileHandlerException e) {
			promptMsg("Error", e.getMessage());
		}
	}


	@Override 
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (position == 0) {
        	promptForString("N�mero nueva p�gina", true);
        }
        else {
        	Intent myIntent = new Intent(this, PageActivity.class);
        	myIntent.putExtra("docName", docName);
        	myIntent.putExtra("pageNum", adapterArray.get(position));
        	startActivity(myIntent);
        }
    }
	
	@Override
	protected void callback (String inputText) {
		try {
			PrintFileHandler.insertPage(inputText);
			adapterArray.add(inputText);
			adapter.notifyDataSetChanged();
		} catch (PrintFileHandlerException e) {
			promptMsg("Error", e.getMessage());
		}
	}

	
//	/**
//	 * Set up the {@link android.app.ActionBar}, if the API is available.
//	 */
//	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
//	private void setupActionBar() {
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//			getActionBar().setDisplayHomeAsUpEnabled(true);
//		}
//	}
//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.doc, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		switch (item.getItemId()) {
//		case android.R.id.home:
//			// This ID represents the Home or Up button. In the case of this
//			// activity, the Up button is shown. Use NavUtils to allow users
//			// to navigate up one level in the application structure. For
//			// more details, see the Navigation pattern on Android Design:
//			//
//			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
//			//
//			NavUtils.navigateUpFromSameTask(this);
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}

}
