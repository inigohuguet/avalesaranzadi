package info.aranzadi2015.avalesaranzadi;

import info.aranzadi2015.avalesaranzadi.PrintFileHandler.PrintFileHandlerException;

import java.io.File;
import java.util.ArrayList;

import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AvalesAranzadiBaseActivity {
	
	ArrayList<String> adapterArray;
	ArrayAdapter<String> adapter;
	Context _this = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("Documentos");
		adapterArray = new ArrayList<String>();
		adapterArray.add("--- A�adir nuevo documento");
		File f = new File(PrintFileHandler.BASE_DIR);
		if (!f.exists()) {
			if (!f.mkdir())
				promptMsg("Error", "No se puede crear el directorio");
		}
		File []fList = f.listFiles();
		if (fList != null)
			for (File f2 : f.listFiles())
				adapterArray.add(f2.getName());
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, adapterArray);
		setListAdapter(adapter);
	}
	
	@Override 
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (position == 0) {
        	promptForString("Nombre nuevo documento");
        }
        else {
        	Intent myIntent = new Intent(this, DocActivity.class);
        	myIntent.putExtra("docName", adapterArray.get(position));
        	startActivity(myIntent);
        }
    }

	@Override
	protected void callback (String inputText) {
		try {
			PrintFileHandler.createDoc(inputText);
			adapterArray.add(inputText);
			adapter.notifyDataSetChanged();
		} catch (PrintFileHandlerException e) {
			promptMsg("Error", e.getMessage());
		}
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		return true;
//	}
//
}
