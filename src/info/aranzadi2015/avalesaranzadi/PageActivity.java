package info.aranzadi2015.avalesaranzadi;

import java.util.ArrayList;

import info.aranzadi2015.avalesaranzadi.PrintFileHandler.PrintFileHandlerException;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PageActivity extends AvalesAranzadiBaseActivity {

	ArrayList<String> adapterArray;
	ArrayAdapter<String> adapter;
	String docName, pageNum;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_page);
		try {
			docName = getIntent().getExtras().getString("docName");
			pageNum = getIntent().getExtras().getString("pageNum");
			setTitle("P�gina: " + pageNum);
			adapterArray = new ArrayList<String>();
			adapterArray.add("--- A�adir nuevo DNI");
			adapterArray.addAll(PrintFileHandler.getDNIs(pageNum));
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, adapterArray);
			setListAdapter(adapter);
		} catch (PrintFileHandlerException e) {
			promptMsg("Error", e.getMessage());
		}
	}


	@Override 
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (position == 0) {
        	int max = 0;
        	for (int i = 1; i < adapterArray.size(); i++)
        		if (Integer.parseInt(adapterArray.get(i)) > max)
        			max = Integer.parseInt(adapterArray.get(i));
        	String dni = Integer.toString(max + 1);
    		try {
				PrintFileHandler.insertDNI(pageNum, dni);
				adapterArray.add(dni);
				adapter.notifyDataSetChanged();
    		} catch (PrintFileHandlerException e) {
    			promptMsg("Error", e.getMessage());
    		}
        }
        else {
        	Intent myIntent = new Intent(this, DniActivity.class);
        	myIntent.putExtra("docName", docName);
        	myIntent.putExtra("pageNum", pageNum);
        	myIntent.putExtra("dni", adapterArray.get(position));
        	startActivity(myIntent);
        }
    }

	
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.page, menu);
//		return true;
//	}

}
