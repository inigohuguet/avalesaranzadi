package info.aranzadi2015.avalesaranzadi;

import info.aranzadi2015.avalesaranzadi.PrintFileHandler.PrintFileHandlerException;

import java.io.File;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class DniActivity extends AvalesAranzadiBaseActivity {

	ArrayList<String> adapterArray;
	ArrayAdapter<String> adapter;
	String docName, pageNum, dni;
	String fileName;
	boolean allowMoreImages;

	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_page);
		docName = getIntent().getExtras().getString("docName");
		pageNum = getIntent().getExtras().getString("pageNum");
		dni = getIntent().getExtras().getString("dni");
		setTitle("P�g " + pageNum +  " DNI #" + dni);
		adapterArray = new ArrayList<String>();
		refreshArrayAdapter(false);
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, adapterArray);
		setListAdapter(adapter);
	}

	protected void refreshArrayAdapter (boolean notify) {
		try {
			ArrayList<String> arr = PrintFileHandler.getDNIImages(pageNum, dni);
			adapterArray.clear();
			if (arr.size() >= 2) {
				allowMoreImages = false;
				adapterArray.addAll(arr);
			}
			else {
				allowMoreImages = true;
				adapterArray.add("--- A�adir imagen");
				adapterArray.addAll(arr);
			}
			if (notify)
				adapter.notifyDataSetChanged();
		} catch (PrintFileHandlerException e) {
			promptMsg("Error", e.getMessage());
		}
	}

	@Override 
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (position == 0 && allowMoreImages) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            fileName = getNewImageFileName();
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(PrintFileHandler.getImgDir(), fileName))); // set the image file name
            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        }
        else {
        	String img = adapterArray.get(position);
        	Uri uri = Uri.fromFile(new File(PrintFileHandler.getDocDir(), img));
        	Uri hacked_uri = Uri.parse("file://" + uri.getPath());
        	Intent myIntent = new Intent();
        	myIntent.setDataAndType(hacked_uri, "image/*");
        	startActivity(myIntent);
        }
    }
	
	protected String getNewImageFileName () {
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		return "page-" + pageNum + "-" + timeStamp + ".jpg";
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode != CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE)
			return;
		if (resultCode == RESULT_OK) {
			Toast.makeText(this, "Imagen guardada: " + fileName, Toast.LENGTH_LONG).show();
			try {
				PrintFileHandler.insertDNIImage(pageNum, dni, "img/"+fileName);
				refreshArrayAdapter(true);
			} catch (PrintFileHandlerException e) {
				promptMsg("Error", "No se pudo guardar la imagen");
			}
		}
		else if (resultCode == RESULT_CANCELED) {
			Toast.makeText(this, "NO SE GUARD� LA IMAGEN", Toast.LENGTH_LONG).show();
		}
		else {
			promptMsg("Error", "No se pudo guardar la imagen");
		}
	}

}
