package info.aranzadi2015.avalesaranzadi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.os.Environment;

public class PrintFileHandler {
	
	public final static String BASE_DIR = Environment.getExternalStorageDirectory().toString()+"/AvalesAranzadi";
	
	public static class PrintFileHandlerException extends Exception {
		public PrintFileHandlerException() { super(); }
		public PrintFileHandlerException(String message) { super(message); }
	}
	
	private static String docPath;
	private static DocumentBuilder docBuilder;
	private static Document xmlDoc;
	private static XPath xpath;
	private static Transformer transformer;
	static {
		try {
			transformer = TransformerFactory.newInstance().newTransformer();
			docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			xpath = XPathFactory.newInstance().newXPath();
			docPath = null;
			xmlDoc = null;
		}
		catch (ParserConfigurationException e) {}
		catch (TransformerConfigurationException e) {}
		catch (TransformerFactoryConfigurationError e) {}
	}
	
	
	// PUBLIC METHODS
	
	public static String getDocDir () {
		return docPath;
	}
	
	public static String getFilePath () {
		return new File(docPath, "print.html").getAbsolutePath();
	}
	
	public static String getImgDir () {
		return new File(docPath, "img").getAbsolutePath();
	}
	
	public static void createDoc (String docName) throws PrintFileHandlerException {
		if (docName.length() == 0 || !docName.matches("[a-zA-Z0-9]+"))
			throw new PrintFileHandlerException("Solo caracteres alfanum�ricos. Sin espacios, ni tildes, ni e�es");
		File dir = new File (BASE_DIR, docName);
		String path = dir.getAbsolutePath();
		if (dir.exists())
			throw new PrintFileHandlerException("El documento ya existe");
		File subdir = new File (path, "img");
		if (!dir.mkdir() || !subdir.mkdir())
			throw new PrintFileHandlerException("No se pudo crear el documento");
		try {
			PrintWriter writer;
			writer = new PrintWriter(new File (path, "print.html"));
			writer.println("<html>");
			writer.println("<style>");
			writer.println(".page { page-break-before: always; width: 90%; }");
			writer.println("img { width: 9.5cm; }");
			writer.println("</style>");
			writer.println("</html>");
			writer.flush();
			writer.close();
		}
		catch (FileNotFoundException e) { throw new PrintFileHandlerException("No se pudo crear el documento"); }
	}

	
	public static void openDoc (String docName) throws PrintFileHandlerException {
		try {
			docPath = new File(BASE_DIR, docName).getAbsolutePath();
			File dir = new File (docPath);
			File file = new File (getFilePath());
			if (!dir.exists() || !dir.isDirectory() || !file.exists())
				throw new PrintFileHandlerException("El documento no existe");
			InputStream stream = new FileInputStream(file);
			xmlDoc = docBuilder.parse(stream);
			stream.close();
		}
		catch (SAXException e) { throw new PrintFileHandlerException("No se pudo leer el documento"); }
		catch (IOException e)  { throw new PrintFileHandlerException("No se pudo leer el documento"); }
	}

	
	public static ArrayList<String> getPages () throws PrintFileHandlerException {
		NodeList nodes = getNodeList("//*[@class='page']");
		if (nodes == null)
			throw new PrintFileHandlerException("No se pudo leer el documento");
		ArrayList<String> arr = new ArrayList<String>();
		for (int i = 0; i < nodes.getLength(); i++)
			arr.add( ((Element)nodes.item(i)).getAttribute("attr-page") );
		return arr;
	}
	
	
	public static void insertPage (String page) throws PrintFileHandlerException {
		if (existElem("//*[@class='page'][@attr-page='"+page+"']"))
			throw new PrintFileHandlerException("La página ya existe");
		Element html = getElem("/html");
		Element elem = xmlDoc.createElement("table");
		elem.setAttribute("class", "page");
		elem.setAttribute("attr-page", page);
		html.appendChild(elem);
		saveXML();
	}
	
	
	public static ArrayList<String> getDNIs (String page) throws PrintFileHandlerException {
		NodeList nodes = getNodeList("//*[@class='page'][@attr-page='"+page+"']//*[@class='dni']");
		if (nodes == null)
			throw new PrintFileHandlerException("No se encuentra la página en el documento");
		ArrayList<String> arr = new ArrayList<String>();
		for (int i = 0; i < nodes.getLength(); i++)
			arr.add( ((Element)nodes.item(i)).getAttribute("attr-dni") );
		return arr;
	}
	
	
	public static void insertDNI (String page, String dni) throws PrintFileHandlerException {
		if (existElem("//*[@class='page'][@attr-page='"+page+"']//*[@class='dni'][@attr-dni='"+dni+"']"))
			throw new PrintFileHandlerException("El DNI ya estaba creado");
		Element pageElem = getElem("//*[@class='page'][@attr-page='"+page+"']");
		Element elem = xmlDoc.createElement("tr");
		elem.setAttribute("class", "dni");
		elem.setAttribute("attr-dni", dni);
		pageElem.appendChild(elem);
		saveXML();
	}
	
	
	public static ArrayList<String> getDNIImages (String pageNum, String dni) throws PrintFileHandlerException {
		NodeList nodes = getNodeList("//*[@class='page'][@attr-page='"+pageNum+"']//*[@class='dni'][@attr-dni='"+dni+"']//*[@class='dni-img']");
		if (nodes == null)
			throw new PrintFileHandlerException("No se encuentra el DNI en el documento");
		ArrayList<String> arr = new ArrayList<String>();
		for (int i = 0; i < nodes.getLength(); i++)
			arr.add( ((Element)nodes.item(i)).getAttribute("src") );
		return arr;
	}
	
	
	public static void insertDNIImage (String pageNum, String dni, String src) throws PrintFileHandlerException {
		if (existElem("//*[@class='page'][@attr-page='"+pageNum+"']//*[@class='dni'][@attr-dni='"+dni+"']" +
											"//*[@class='dni-img'][@src='"+src+"']"))
			throw new PrintFileHandlerException("La imagen ya estaba creada");
		Element dniElem = getElem("//*[@class='page'][@attr-page='"+pageNum+"']//*[@class='dni'][@attr-dni='"+dni+"']");
		Element col = xmlDoc.createElement("td");
		Element imgElem = xmlDoc.createElement("img");
		imgElem.setAttribute("class", "dni-img");
		imgElem.setAttribute("src", src);
		dniElem.appendChild(col);
		col.appendChild(imgElem);
		saveXML();
	}
		
	
	// PRIVATE HELPER METHODS
	
	private static NodeList getNodeList (String xpathExpr) {
		try {
			XPathExpression expr = xpath.compile(xpathExpr);
			NodeList nodes = (NodeList) expr.evaluate(xmlDoc, XPathConstants.NODESET);
			return nodes;
		} catch (XPathExpressionException e) {
			return null;
		}
	}
	
	private static boolean existElem (String xpathExpr) throws PrintFileHandlerException {
		NodeList nodes = getNodeList(xpathExpr);
		if (nodes == null) throw new PrintFileHandlerException("No se pudo leer el documento");
		return 0 != nodes.getLength();
	}
	
	private static Element getElem (String xpathExpr) throws PrintFileHandlerException {
		NodeList nodes = getNodeList(xpathExpr);
		if (nodes == null) throw new PrintFileHandlerException("No se pudo leer el documento");
		return (Element) nodes.item(0);
	}
	
	private static void saveXML() throws PrintFileHandlerException {
		try {
			DOMSource domSrc = new DOMSource(xmlDoc);
			StreamResult streamResult = new StreamResult(new File(getFilePath()));
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
//			transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "-//W3C//DTD XHTML 1.0 Transitional//EN");
//			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			transformer.transform(domSrc, streamResult);
		} catch (TransformerException e) {
			throw new PrintFileHandlerException("No se pudo guardar el documento");
		}
	}
	
	
	
	public static void main (String [] args) {
		try {
			ArrayList <String> arr, arr2, arr3;
			
			// create base dir
			File baseDir = new File(BASE_DIR);
			if (!baseDir.exists())
				baseDir.mkdir();
			
			// doc 1
//			try{
//				createDoc("doc1");
//			} catch (PrintFileHandlerException e){e.printStackTrace();}
			openDoc("doc1");
//			try{
//				insertPage("156");
//			} catch (PrintFileHandlerException e){e.printStackTrace();}
//			try{
//				insertDNI("156", "DNI 1");
//			} catch (PrintFileHandlerException e){e.printStackTrace();}
//			try{
//				insertDNIImage("156", "DNI 1", "img/img1.jpg");
//			} catch (PrintFileHandlerException e){e.printStackTrace();}
//			insertDNIImage("156", "DNI 1", "img/img2.jpg");
//			insertDNI("156", "DNI 2");
//			insertDNIImage("156", "DNI 2", "img/img3.jpg");
//			insertDNIImage("156", "DNI 2", "img/img4.jpg");
//			insertPage("157");
//			insertDNI("157", "DNI 48");
//			insertDNIImage("157", "DNI 48", "img/img48.jpg");
//			insertDNIImage("157", "DNI 48", "img/img49.jpg");
			
			arr = getPages();
			for (String page : arr) {
				System.out.println("page " + page);
				arr2 = getDNIs(page);
				for (String dni : arr2) {
					System.out.println("\t- dni: " + dni);
					arr3 = getDNIImages(page, dni);
					for (String img : arr3)
						System.out.println("\t\t- img: " + img);
				}
			}
			
			// doc
//			createDoc("doc2");
			openDoc("doc2");
//			insertPage("12");
//			insertDNI("12", "DNI 1");
//			insertDNIImage("12", "DNI 1", "img/img1.jpg");
//			insertDNIImage("12", "DNI 1", "img/img2.jpg");

			arr = getPages();
			for (String page : arr) {
				System.out.println("page " + page);
				arr2 = getDNIs(page);
				for (String dni : arr2) {
					System.out.println("\t- dni: " + dni);
					arr3 = getDNIImages(page, dni);
					for (String img : arr3)
						System.out.println("\t\t- img: " + img);
				}
			}

		} catch (PrintFileHandlerException e) {
			e.printStackTrace();
		}
		
	}

}
